var express = require('express');
var router = express.Router();

/* GET campaigns listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a campaign resource');
});

module.exports = router;
